# Portfolio Website

Welcome to the repository for my personal [porfolio website](https://portfolio.manuel-thoene.de).

This project showcases my skills, projects, and experiences in the world of web development. The portfolio is built using the Svelte framework, enhanced with the Skeleton UI-Library and Tailwind CSS.

As a passionate web developer, I wanted to create a platform to showcase my journey, skills, and the projects I've been working on. This portfolio website serves as an interactive resume and portfolio combined, providing visitors with insights into my expertise and the projects I'm proud of.

Thank you for taking the time to explore my portfolio website repository. I hope you enjoy browsing through my work and learning more about me as a web developer. If you have any questions or feedback, don't hesitate to reach out!
