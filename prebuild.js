import path from 'path';
import fs from 'fs'

// List of directories to remove
const directoriesToRemove = [
	'./.svelte-kit/output/',
];

directoriesToRemove.forEach(directory => {
	const dir = path.normalize(directory);

	if (fs.existsSync(dir)) {
		fs.rm(dir,
			{ recursive: true, force: true }, (err) => {

				if (err) {
					return console.error(`Error removing ${dir} directory: ${err.message}`);
				}
				console.log(`Directory ${dir} deleted successfully`);
			});
	}
});
