import { writable, get } from 'svelte/store';

/** Used only for importing the sources
 * 	@param key key for source
 * 	@param value text for citation
 */
export type CitationSource = Record<string, string>;

export interface CitationSourceObj {
	position: number;
	citation: string;
}

export interface Library {
	activeSourceKeys: string[];
	allSources: Map<string, CitationSourceObj>;
	hoverSource: CitationSourceObj | undefined;
}

/** Parses the text and returns a string with anchor elements for urls.
 * @see https://stackoverflow.com/a/8943487 */
function parseCitation(c: string): string {
	if (!c) return '';

	const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])/gi;
	return c.replace(urlRegex, function (url) {
		return '<a class="link" href="' + url + '" target="_blank">' + url + '</a>';
	});
}

function createLibrary() {
	const { subscribe, set, update } = writable<Library | undefined>();

	return {
		subscribe,
		setSources: (sources: CitationSource) => {
			if (sources) {
				const keys = Object.keys(sources);
				const newLibrary: Library = {
					activeSourceKeys: [],
					allSources: new Map(),
					hoverSource: undefined,
				};

				for (const key of keys) {
					const value = sources[key];
					const sourceObj: CitationSourceObj = {
						position: -1,
						citation: parseCitation(value)
					};

					newLibrary.allSources.set(key, sourceObj);
				}

				set(newLibrary);
			}
		},
		setSourceActive: (sourceKey: string) =>
			update((library) => {
				if (library?.allSources.has(sourceKey)) {
					const source = library.allSources.get(sourceKey) as CitationSourceObj;
					if (source.position < 0) {
						const index = library.activeSourceKeys.push(sourceKey) - 1;
						source.position = index;
					}
				}
				return library;
			}),
		setSourceHover: (sourceKey: string) =>
			update((library) => {
				if (library?.allSources.has(sourceKey)) {
					const source = library.allSources.get(sourceKey) as CitationSourceObj;
					library.hoverSource = source;			
				}
				
				return library;
			}),
		destroy: () => set(undefined),
		getHoverSource: () => get(library)?.hoverSource,
	};
}

export const library = createLibrary();
