import type { Writable } from 'svelte/store';
import { get } from 'svelte/store';
import { localStorageStore } from '@skeletonlabs/skeleton';


export const fallBackLanguage = 'de'; // Also set in app.html!

export type Translation = {
    de: string,
    en: string,
}

export const supportedLanguages = new Map<string, string>([
	['en', 'EN'],
	['de', 'DE']
]);

export const langOsPrefers: Writable<string> = localStorageStore(
	'langOsPrefers',
	fallBackLanguage
);
export const langUserPrefers: Writable<string | undefined> = localStorageStore(
	'langUserPrefers',
	undefined
);
export const langCurrent: Writable<string> = localStorageStore(
	'langCurrent',
	fallBackLanguage
);

export function getLangOsPrefers() {
	const langOsPrefersStr = navigator.language.toLocaleLowerCase().split('-')[0];

	if (langOsPrefersStr && supportedLanguages.has(langOsPrefersStr)) {
		const langToSet = langOsPrefersStr;
		langOsPrefers.set(langToSet);
	}
	return langOsPrefersStr;
}

export function getLangUserPrefers() {
	return get(langUserPrefers);
}

export function getLangAutoPrefers() {
	const os = getLangOsPrefers();
	const user = getLangUserPrefers();
	const langValue = user !== undefined ? user : os;
	return langValue;
}

export function setLangUserPrefers(value: string) {
	if (supportedLanguages.has(value)) {
		langUserPrefers.set(value);
		return;
	}
	console.error(`Language '${value}' is not supported by this website.`);
}

export function setLangCurrent(value: string) {
	if (supportedLanguages.has(value)) {
		document.documentElement.lang = value;
		langCurrent.set(value);
		return;
	}
	console.error(`Language '${value}' is not supported by this website.`);
}

/** Set the language on page load. */
export function setInitialLangState() {
	const langUserPrefers = localStorage.getItem('langUserPrefers');
	const doesCurrentMatchSaved = document.documentElement.lang.toLocaleLowerCase().split('-')[0] === langUserPrefers;

	if (langUserPrefers && !doesCurrentMatchSaved) {
		document.documentElement.lang = langUserPrefers;
	}
}