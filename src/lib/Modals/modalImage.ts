import { modalStore, type ModalSettings } from '@skeletonlabs/skeleton';

function modalComponentImage(
	image: string,
	title: string | undefined,
	alt: string | undefined
): void {
	const modal: ModalSettings = {
		type: 'component',
		component: 'ModalImageComponent',
		backdropClasses: '!bg-surface-10/95 dark:!bg-surface-800/95',
		image: image,
		meta: { title: title, alt: alt }
	};
	modalStore.trigger(modal);
}

export default function handleImageInteraction(event: MouseEvent | KeyboardEvent) {
	const keyEvent = event as KeyboardEvent;

	if (event instanceof KeyboardEvent && keyEvent.code != 'Enter' && keyEvent.code != 'Space') {
		return;
	}

	if (!(event.target instanceof HTMLImageElement)) {
		return;
	}

	const image = event.target.src;
	const title = event.target.title;
	const alt = event.target.alt;

	modalComponentImage(image, title, alt);
}
