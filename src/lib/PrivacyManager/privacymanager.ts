import type { Writable } from 'svelte/store';
import { get, writable } from 'svelte/store';
import { localStorageStore } from '@skeletonlabs/skeleton';

export enum privacyLevels {
	unset,
	allowNecessary,
	allowFunctional,
	allowAll,
}

export const hideModalOnRoutes = ['/imprint', '/privacy'];

export const activePrivacyLevels = writable<privacyLevels[]>([privacyLevels.unset, privacyLevels.allowNecessary]);

export const hideModal = writable(false);

export function autoSetShowModal(currentPath:string) {	
	hideModal.set(hideModalOnRoutes.includes(currentPath));
}

export function getHideModal() {
	return get(hideModal);
}

export const privBrowserPrefers: Writable<number> = localStorageStore(
	'privBrowserPrefers',
	privacyLevels.unset
);

export const privUserPrefers: Writable<number> = localStorageStore(
	'privUserPrefers',
	privacyLevels.unset
);
export const privCurrent: Writable<number> = localStorageStore(
	'privCurrent',
	privacyLevels.unset
);

export function getActivePrivacyLevels():privacyLevels[] {
	return get(activePrivacyLevels);
}

export function getPrivBrowserPrefers() {
	let browserPrefers = privacyLevels.unset;

	if (navigator.doNotTrack || window.doNotTrack) {
		if (
		  window.doNotTrack === "1" ||
		  navigator.doNotTrack === "1" ||
		  navigator.doNotTrack === "yes"
		) {
		  // the options is enabled
		  browserPrefers = privacyLevels.allowNecessary;
		}
	  }

	privBrowserPrefers.set(browserPrefers);
	return browserPrefers;
}

export function getPrivUserPrefers() {
	return get(privUserPrefers);
}

export function setActivePrivacyLevels(levels: privacyLevels[]) {
	if(levels.length < 1) return;

	const levelsToSet:privacyLevels[] = [];

	levels.forEach( (l) => {
		if (Object.values(privacyLevels).includes(l)) {
			levelsToSet.push(l);
		} else {
			console.error(`Couldn't set privacy level '${l}'.`);
		}
	});
	activePrivacyLevels.set(levelsToSet);
}

export function getPrivAutoPrefers() {
	const browser = getPrivBrowserPrefers();
	const user = getPrivUserPrefers();
	const prefValue = user !== privacyLevels.unset ? user : browser;
	return prefValue;
}

export function setPrivUserPrefers(value: number) {
	if (Object.values(privacyLevels).includes(value)) {
		privUserPrefers.set(value);
		return;
	}
	console.error(`Privacy level '${value}' is not supported by this website.`);
}

export function setPrivCurrent(value: number) {
	if (Object.values(privacyLevels).includes(value)) {
		document.documentElement.dataset.privacyLevel = value.toString();
		privCurrent.set(value);
		return;
	}
	console.error(`Privacy level '${value}' is not supported by this website.`);
}

/** Set the language on page load. */
export function setInitialPrivState() {
	const privUserPrefers = localStorage.getItem('privUserPrefers');
	const doesCurrentMatchSaved = document.documentElement.dataset.privacyLevel === privUserPrefers;

	if (privUserPrefers && !doesCurrentMatchSaved) {
		document.documentElement.dataset.privacyLevel = privUserPrefers;
	}
}