import { modalStore, type ModalSettings } from '@skeletonlabs/skeleton';
import { getHideModal } from '$lib/PrivacyManager/privacymanager';




function modalComponentPrivacy(
): void {
	const modal: ModalSettings = {
		type: 'component',
		component: 'ModalPrivacyComponent',
		backdropClasses: '!bg-surface-10/70 dark:!bg-surface-900/90 pointer-events-none',
	};
	
	if(!getHideModal()) {
		modalStore.trigger(modal);
	}
	
}

export default function handlePrivacyInteraction() {
	modalComponentPrivacy();
}
