import { writable } from 'svelte/store';

const baseTitle = 'Manuel Thöne – Portfolio';

function createTitle() {
	const {subscribe, set} = writable('');
	
	return {
		subscribe,
		set: (value: string) => {
			set(`${value} | ${baseTitle}`)
		},
		clear: () => {
			set(baseTitle);
		}
	}
}

export const title = createTitle();