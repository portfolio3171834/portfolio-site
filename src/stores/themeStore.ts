import { readable } from 'svelte/store';

interface ThemeParameters {
	[key: string]: string;
}

class ThemeManager {
	private themeFonts: ThemeParameters = {
		'theme-font-family-base': '"Nunito", sans-serif, system-ui',
		'theme-font-family-heading': '"Josefin Sans", system-ui, sans-serif'
	};

	private themeSettings: ThemeParameters = {
		'theme-rounded-base': '9999px',
		'theme-rounded-container': '8px',
		'theme-border-base': '1px'
	};

	private themeColors: ThemeParameters = {
		'theme-font-color-base': '0 0 0',
		'theme-font-color-dark': '255 255 255',
		'on-primary': '0 0 0',
		'on-secondary': '0 0 0',
		'on-tertiary': '0 0 0',
		'on-success': '0 0 0',
		'on-warning': '0 0 0',
		'on-error': '0 0 0',
		'on-surface': '255 255 255',
		'color-primary-50': '223 240 249',
		'color-primary-100': '213 235 247',
		'color-primary-200': '202 230 245',
		'color-primary-300': '171 215 238',
		'color-primary-400': '107 184 226',
		'color-primary-500': '44 154 213',
		'color-primary-600': '40 139 192',
		'color-primary-700': '33 116 160',
		'color-primary-800': '26 92 128',
		'color-primary-900': '22 75 104',
		'color-secondary-10': '255 253 250',
		'color-secondary-50': '255 242 223',
		'color-secondary-100': '255 237 213',
		'color-secondary-200': '255 233 202',
		'color-secondary-300': '255 219 170',
		'color-secondary-400': '255 193 107',
		'color-secondary-500': '255 166 43',
		'color-secondary-600': '230 149 39',
		'color-secondary-700': '191 125 32',
		'color-secondary-800': '153 100 26',
		'color-secondary-900': '125 81 21',
		'color-tertiary-50': '252 251 251',
		'color-tertiary-100': '251 250 249',
		'color-tertiary-200': '251 249 248',
		'color-tertiary-300': '248 245 244',
		'color-tertiary-400': '242 238 235',
		'color-tertiary-500': '237 231 227',
		'color-tertiary-600': '213 208 204',
		'color-tertiary-700': '178 173 170',
		'color-tertiary-800': '142 139 136',
		'color-tertiary-900': '116 113 111',
		'color-success-50': '227 246 226',
		'color-success-100': '218 243 217',
		'color-success-200': '209 240 207',
		'color-success-300': '181 230 178',
		'color-success-400': '126 212 121',
		'color-success-500': '70 193 63',
		'color-success-600': '63 174 57',
		'color-success-700': '53 145 47',
		'color-success-800': '42 116 38',
		'color-success-900': '34 95 31',
		'color-warning-50': '255 238 219',
		'color-warning-100': '255 233 207',
		'color-warning-200': '255 227 195',
		'color-warning-300': '255 210 159',
		'color-warning-400': '255 177 87',
		'color-warning-500': '255 143 15',
		'color-warning-600': '230 129 14',
		'color-warning-700': '191 107 11',
		'color-warning-800': '153 86 9',
		'color-warning-900': '125 70 7',
		'color-error-50': '255 231 232',
		'color-error-100': '255 223 224',
		'color-error-200': '255 215 217',
		'color-error-300': '255 191 194',
		'color-error-400': '255 142 148',
		'color-error-500': '255 94 102',
		'color-error-600': '230 85 92',
		'color-error-700': '191 71 77',
		'color-error-800': '153 56 61',
		'color-error-900': '125 46 50',
		'color-surface-10': '249 250 250',
		'color-surface-50': '225 226 230',
		'color-surface-100': '216 217 222',
		'color-surface-200': '206 207 214',
		'color-surface-300': '176 179 189',
		'color-surface-400': '117 121 140',
		'color-surface-500': '58 64 90',
		'color-surface-600': '52 58 81',
		'color-surface-700': '44 48 68',
		'color-surface-800': '35 38 54',
		'color-surface-900': '28 31 44'
	};

	private getRgbChannels(colorName: string): number[] | undefined {
		let rgbChannels: number[];
		try {
			const rawColor = this.themeColors[colorName];
			rgbChannels = rawColor.split(' ').map((channel) => parseInt(channel, 10));
			
			if (rgbChannels.length !== 3 || rgbChannels.some(isNaN)) {
				throw new Error(`Invalid RGB format for color '${colorName}'`);
			}
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		} catch (error: any) {
			console.error(error.message);
			return undefined;
		}
		return rgbChannels;
	}

	getRgbColor(colorName: string): string | undefined {
			const rgbChannels = this.getRgbChannels(colorName);
			if(!rgbChannels) return undefined;
			return `rgb(${rgbChannels.join(', ')})`;
	}

	getHexColor(colorName: string): string | undefined {
			const rgbChannels = this.getRgbChannels(colorName);

			if(!rgbChannels) return undefined;

			const hexChannels = rgbChannels.map((channel) => {
				const hexValue = Math.min(255, Math.max(0, channel)).toString(16).toUpperCase();
				return hexValue.length === 1 ? `0${hexValue}` : hexValue;
			});

			return `#${hexChannels.join('')}`;
	}

	getFont(propertyName: string): string | undefined {
		return this.themeFonts[propertyName];
	}

	getSetting(propertyName: string): string | undefined {
		return this.themeSettings[propertyName];
	}
}

const themeManager = new ThemeManager();
export const themeStore = readable(themeManager);
