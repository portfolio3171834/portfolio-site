import { join } from 'path'
import typography from '@tailwindcss/typography'
import skeleton from '@skeletonlabs/skeleton/tailwind/skeleton.cjs'
import plugin from 'tailwindcss/plugin';



/** @type {import('tailwindcss').Config} */
module.exports = {
	darkMode: 'class',
	content: ['./src/**/*.{html,js,svelte,ts}', join(require.resolve('@skeletonlabs/skeleton'), '../**/*.{html,js,svelte,ts}')],
	theme: {
		extend: {
			fontFamily: {
				nunito: ["Nunito", "sans-serif"],
			},
			colors: {
				'surface': {
					10: '#f9fafa'
				},
				'secondary': {
					10: '#fffdfa'
				}
			},
			zIndex: {
				'1': '1',
			},
		},
	},
	plugins: [typography, ...skeleton(), plugin(function ({ addBase }) {
		addBase({
			'html': { fontSize: "18px"},
		})
	}),],
}
